#! /bin/sh

# Get all
# curl localhost:8080/api/item

# Get one
# curl localhost:8080/api/item/50ad8f87ef86fabb0eef265c

# Search for cat food
# curl localhost:8080/api/item/search/Cat

# Delete cat food
# curl -X DELETE localhost:8080/api/item/50ad8f87ef86fabb0eef265c

# Add it back
# curl -X POST -H "Content-type: application/json" localhost:8080/api/item -d '{"name":"Cat Food","description":"Yummy, tasty cat food","price":4.25,"taxable":true,"weightInGrams":1000,"qnty":4}'

# Change it
# curl -X PUT -H "Content-type: application/json" localhost:8080/api/item/50ad8f87ef86fabb0eef265e -d '{"name":"fishz Food"}'
