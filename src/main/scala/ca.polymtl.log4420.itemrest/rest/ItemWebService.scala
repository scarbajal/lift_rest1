package ca.polymtl.log4420.itemrest
package rest

import model.Item

// Lift
import net.liftweb._
import common.Box

// Lift http
import http.rest.RestHelper
import http.S

// Lift json
import json._

/**
 * A full REST example
 */
object ItemWebService extends RestHelper
{
  // Serve /api/item and friends
  serve( "api" / "item" prefix {

    // /api/item returns all the items
    case Nil JsonGet _ => Item.findAll: JValue

    // /api/item/item_id gets the specified item (or a 404)
    case itemId :: Nil JsonGet _ => Item.findById( itemId ): Box[JValue]

    // /api/item/search or /api/item/search?q=foo
    case "search" :: query JsonGet _ => Item.findByKeywords( query ::: S.params("q") ): JValue
  })
}
